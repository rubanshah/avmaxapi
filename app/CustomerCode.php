<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerCode extends Model
{
    protected $table = 'customer_codes';

    protected $fillable = [
        'id', 'customer_id', 'customer_code', 'division_code', 'application_code', 'name', 'type'
    ];

    public  function customer(){
        return $this->belongsTo('App\Customer','customer_id','id');
    }
}
