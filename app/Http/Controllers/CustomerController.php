<?php
/**
 * Created by PhpStorm.
 * User: ruban
 * Date: 4/19/2017
 * Time: 9:09 AM
 */
namespace App\Http\Controllers;
use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;
use App\Comment;
use Excel;
use App\Customer;

class CustomerController extends Controller{

    private $customerRepo;

    function __construct(CustomerRepository $customerRepo){
        $this->customerRepo = $customerRepo;
    }

    function index(Request $request){
        return $this->customerRepo->getAll($request->input('search',null));
    }

    function customersWithRelations(Request $request){
        return Customer::whereNull('parent_id')->orderBy('name')->with('codes','children.codes')->paginate(5000);
    }

    function parentCustomer(){
        return $this->customerRepo->parentCustomer();
    }

    function getChildren($id){
        return $this->customerRepo->getChildren($id);
    }

    function find($id){
        return $this->customerRepo->findWithCodes($id);
    }

    function create(Request $request){
        return $this->customerRepo->create($request->all());
    }

    function update($id, Request $request){
        return $this->customerRepo->update($id,$request->all());
    }

    function delete($id){
        return $this->customerRepo->delete($id);
    }

    function getComments($customerId){
        return $this->customerRepo->getComments($customerId);
    }

    function createComment($customerId, Request $request){
        $data = $request->only('text');
        return $this->customerRepo->createComment($customerId, $data);

    }

    function deleteComment(Comment $id){
        return $this->customerRepo->deleteComment($id->id);
    }

    function getComment($id){
        return $this->customerRepo->getComment($id);
    }

    function updateComment(Comment $id, Request $request){
        $data = $request->only('text');
        return $this->customerRepo->updateComment($id->id,$data);
    }

    function exportCustomerList(){
        $parentRowColor = '#008000';
        $childRowColor = '#32CD32';



        $data = $this->customerRepo->exportCustomerList();

        Excel::create('customers', function($excel) use($data){

                     $excel->sheet('customers', function($sheet) use($data) {

                        $i=1;
                        foreach($data as $customer){
                            $row = [
                            'id'=>$customer->id,
                            'parent_id'=>$customer->parent_id,
                            'name'=>$customer->name
                            ];

                            $sheet->row($i,$row);
                            $sheet->row($i, function($row) {
                                    $row->setBackground('#008000'); 
                                });

                            $i++;


                            //add codes 

                            foreach($customer->codes as $customerCode){
                                $row = [
                                'id'=>null,
                                'parent_id'=>'code',
                                'name'=>$customerCode->name,
                                'code'=>$customerCode->customer_code,
                                'division'=>$customerCode->division_code
                                ];
                            
                            $sheet->row($i,$row);
                            $i++;
                            }

                            //add children
                         foreach($customer->children as $child){
                            $row = [
                            'id'=>null, //do not include id for child customer
                            'parent_id'=>$child->parent_id,
                            'name'=>$child->name
                            ];

                            $sheet->row($i,$row);
                             $sheet->row($i, function($row) {
                                    $row->setBackground('#e8e863'); 
                                });

                            $i++;

                            //add codes 

                            foreach($child->codes as $customerCode){
                                $row = [
                               'id'=>null,
                                'parent_id'=>'code',
                                'name'=>$customerCode->name,
                                'code'=>$customerCode->customer_code,
                                'division'=>$customerCode->division_code
                                ];
                            
                            $sheet->row($i,$row);
                            $i++;
                            }
                        }
                        //eof add children

                        }});

            })->export('xls');

        return $data;
    }
}