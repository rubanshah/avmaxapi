<?php
/**
 * Created by PhpStorm.
 * User: ruban
 * Date: 4/19/2017
 * Time: 9:09 AM
 */
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Currency;

class SettingsController extends Controller{

    
    function __construct(){
        
    }

    function getCurrencyExchangeRates(){
       return Currency::all();
    }

    function updateCurrencyExchangeRates(Request $request){

        $data = $request->all();

        foreach ($data as $row) {
              $rate = Currency::find($row['id']);
              $rate->fill($row);
              $rate->save();
        }

        return $data;
    }

}