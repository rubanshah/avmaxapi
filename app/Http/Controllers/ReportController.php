<?php
/**
 * Created by PhpStorm.
 * User: ruban
 * Date: 4/19/2017
 * Time: 9:09 AM
 */
namespace App\Http\Controllers;
use App\Customer;
use App\Http\Helpers\Helper;
use App\Repositories\ReportRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use App\CustomerCode;
use App\CashFlow;
use Excel;


class ReportController extends Controller{

    private $reportRepo;

    function __construct(ReportRepository $report){
        $this->reportRepo = $report;
    }

    function aging(Request $request){
        return $this->reportRepo->agingReport($request->all());
    }

    function agingReport(Request $request){

//        $result = DB::select('SELECT COALESCE(parent_id,customer_id) AS customer,c.parent_id,a.customer_id,c.name,a.current,a.thirty_days,a.sixty_days,a.sixty_days_plus,a.currency_code,a.division_code FROM agings a JOIN customers c ON (a.customer_id = c.id) order by parent_id,name asc');

        $query = 'SELECT cc.text AS latest_comment,
                              COALESCE(c.parent_id,a.customer_id) AS customer,c.parent_id,
                              a.customer_id,c.name,a.current,a.thirty_days,a.sixty_days,a.sixty_days_plus,a.currency_code,a.division_code
                              FROM agings a JOIN customers c ON (a.customer_id = c.id)
                              LEFT JOIN (SELECT customer_id,`text` FROM comments WHERE id IN (SELECT MAX(id) FROM comments GROUP BY customer_id)) AS cc ON (c.id = cc.customer_id)';
        $where = [];
        $where[] = "'1' = '1'";

        if($request->input('division_code',null)){
            $division_code = $request->input('division_code');
            $where[] = "a.division_code='$division_code'";
        }

        //apply ACL filter
        if(!Helper::is_admin()){
            $customerIds = Helper::cast_null_to_array(Auth::user()->customers);

            if($customerIds){
                $where[] = " (c.id in(".implode(',',$customerIds).") or c.parent_id in (".implode(',',$customerIds)."))";
            }else{
                $where[] = " c.id in(".implode(',',[0]).")";
            }

            $divisionCodes = Helper::cast_null_to_array(Auth::user()->divisions);

            if($divisionCodes)
            {
                $strDivisionCodes = "'".implode("','",$divisionCodes)."'";
                $where[] = "a.division_code in ($strDivisionCodes)";
            }


        }


        $query = $query.' where '. implode(' and ', $where);

        $query = $query. ' ORDER BY c.parent_id,`name` ASC';

        $result = DB::select($query);

        if($request->input('currency_code',null)){
            $result = $this->transformCollectionToDefaultCurrency(collect($result),true); //here true is passed so that currency code is also changed in collection transform
        }

        $collection = collect($result)->groupBy('customer');

        $data =[];

        foreach($collection as $id=>$customer)
        {

            $data[$id]['name'] = $customer[0]->name;
            $data[$id]['latest_comment'] = $customer[0]->latest_comment;
            $data[$id]['customer_id'] = $id;
            $data[$id]['current'] = 0;
            $data[$id]['thirty_days'] = 0;
            $data[$id]['sixty_days'] =0;
            $data[$id]['sixty_days_plus'] =0;
            $data[$id]['currency_code'] = null;
            $data[$id]['collapsed'] = true;

            $agingsGroupByCustomer = $customer->groupBy('customer_id');

            foreach($agingsGroupByCustomer as $k=>$customerAging){
                $groupByCurrencyCode = $customerAging->groupBy('currency_code');

                foreach($groupByCurrencyCode as $i=>$customerAgingsByCurrencyCode){
                    $aging = new \stdClass();
                    $aging->customer_id = $customerAgingsByCurrencyCode[0]->customer_id;
                    $aging->name = $customerAgingsByCurrencyCode[0]->name;
                    $aging->latest_comment = $customerAgingsByCurrencyCode[0]->latest_comment;
                    $aging->current = $customerAgingsByCurrencyCode->sum->current;
                    $aging->thirty_days = $customerAgingsByCurrencyCode->sum->thirty_days;
                    $aging->sixty_days = $customerAgingsByCurrencyCode->sum->sixty_days;
                    $aging->sixty_days_plus = $customerAgingsByCurrencyCode->sum->sixty_days_plus;
                    $aging->total = $aging->current + $aging->thirty_days + $aging->sixty_days + $aging->sixty_days_plus;

                    $aging->currency_code = $i;
                    $data[$id]['agings'][] =  $aging;
                }
            } //end of foreach grouping via customercode and formatted agings data

            // aggregate for top level customer
            if($this->has_single_currency($customer)){
                $data[$id]['current'] = $customer->sum->current;
                $data[$id]['thirty_days'] = $customer->sum->thirty_days;
                $data[$id]['sixty_days'] = $customer->sum->sixty_days;
                $data[$id]['sixty_days_plus'] = $customer->sum->sixty_days_plus;
                $data[$id]['currency_code'] = $customer[0]->currency_code;
                $data[$id]['total'] = $data[$id]['current']+$data[$id]['thirty_days']+$data[$id]['sixty_days']+$data[$id]['sixty_days_plus'];
            }else{//convert each agings to usd and then sum

                $transformCustomer = $this->transformCollectionToDefaultCurrency(collect($customer));
                $data[$id]['current'] = $transformCustomer->sum->current;
                $data[$id]['thirty_days'] = $transformCustomer->sum->thirty_days;
                $data[$id]['sixty_days'] = $transformCustomer->sum->sixty_days;
                $data[$id]['sixty_days_plus'] = $transformCustomer->sum->sixty_days_plus;
                $data[$id]['currency_code'] = 'USD';
                $data[$id]['total'] = $data[$id]['current']+$data[$id]['thirty_days']+$data[$id]['sixty_days']+$data[$id]['sixty_days_plus'];
            }
            // eof aggregate for top level customer
        } //eof main foreach

        $finalData = []; //to store removed associative index so the returned json data is array instead of object

        $finalData['customers'] = null;
        $finalData['aggregate'] = null;

        foreach($data as $customer){ //to remove associative index to json returns customer as array
            $finalData['customers'][]=$customer;
        }

        if($request->input('currency_code',null)){ //as $result agings have been converted at begining of method
            $finalData['aggregate'] = new \stdClass();
            $finalData['aggregate']->current = $result->sum->current;
            $finalData['aggregate']->thirty_days = $result->sum->thirty_days;
            $finalData['aggregate']->sixty_days = $result->sum->sixty_days;
            $finalData['aggregate']->sixty_days_plus = $result->sum->sixty_days_plus;
            $finalData['aggregate']->currency_code = $request->input('currency_code');

            $finalData['aggregate']->total = $finalData['aggregate']->current + $finalData['aggregate']->thirty_days
                                                + $finalData['aggregate']->sixty_days + $finalData['aggregate']->sixty_days_plus;
        }

        return $finalData;
    }

    function cashFlowReport(){
        return CashFlow::all();
    }


    function has_single_currency($collection){
        return (count($collection->pluck('currency_code')->unique())>1)?false:true;
    }

    function transformCollectionToDefaultCurrency($collection,$modifyCurrencyCode = false){
        return $collection->transform(function($aging,$key) use ($modifyCurrencyCode){
            $aging->current = round(currency($aging->current,$aging->currency_code,'USD',false),2);
            $aging->thirty_days = round(currency($aging->thirty_days,$aging->currency_code,'USD',false),2);
            $aging->sixty_days = round(currency($aging->sixty_days,$aging->currency_code,'USD',false),2);
            $aging->sixty_days_plus = round(currency($aging->sixty_days_plus,$aging->currency_code,'USD',false),2);

            if($modifyCurrencyCode){
                $aging->currency_code = 'USD';
            }

            return $aging;
        });
    }
    //eof aging report formatting





    function validateAging(Request $request){
//        dd($request->all());
//        dd($request->all()->only('customer_code'));
        return $this->reportRepo->validateAging($request->all());
    }

  


    function importAging(Request $request){
        return $this->reportRepo->importAging($request->all());
    }

  
   function exportAgingInvalidData(Request $request){

    $data = $request->all();
    
    $fileName = 'missing-customers-'.$request->user()->id.time();
    Excel::create($fileName, function($excel) use($data){

                     $excel->sheet('customers', function($sheet) use($data){

                        $i=1;
                        foreach($data as $customer){
                            $row = [
                            'division_code'=>$customer['division_code'],
                            'customer_code'=>$customer['customer_code'],
                            'customer_name'=>$customer['customer_name'],
                            'current'=>$customer['current'],
                            'thirty_days'=>$customer['thirty_days'],
                            'sixty_days'=>$customer['sixty_days'],
                            'sixty_days_plus'=>$customer['sixty_days_plus'],
                            'currency_code'=>$customer['currency_code'],
                            ];

                            $sheet->row($i,$row);

                            $i++;
                        }
                        });

            })->store('xls');
    $data = [];
    $data['link'] = url('api/download/'.$fileName.'.xls');
    return $data;
    }


    function exportCashFlowInvalidData(Request $request){

    $data = $request->all();
    
    $fileName = 'missing-customers-'.$request->user()->id.time();
    Excel::create($fileName, function($excel) use($data){

                     $excel->sheet('customers', function($sheet) use($data){

                        $i=1;
                        foreach($data as $customer){
                            $row = [
                            'division_code'=>$customer['division_code'],
                            'name'=>$customer['name'],
                            'amount'=>$customer['amount'],
                            'currency_code'=>$customer['currency_code'],
                            'date'=>$customer['date']
                            ];

                            $sheet->row($i,$row);

                            $i++;
                        }
                        });

            })->store('xls');
    $data = [];
    $data['link'] = url('api/download/'.$fileName.'.xls');
    return $data;
    }


    function validateCashFlow(Request $request){
        return $this->reportRepo->validateCashFlow($request->all());
    }

    function importCashFlow(Request $request){
            return $this->reportRepo->importCashFlow($request->all());
    }

   function customersForCashFlow(Request $request){
      return Customer::where('parent_id','=',null)->orderBy('name','asc')->get();
    }

    
    function customerCashFlow($id, Request $request){
        $query = "select c.name, cf.*, date_format(cf.date,'%Y-%m-%d') as date from cash_flow cf join customers c on(cf.customer_id = c.id) where ";

        $where = [];
        $where[] = "cf.customer_id = $id";

        if($request->input('start_date') && $request->input('end_date')){
            $start_date = $request->input('start_date');
            $end_date = $request->input('end_date');

            $where[] = "cf.date between '$start_date' and '$end_date'";
        }

        if($request->input('division_code')){
            $division_code = $request->input('division_code');
            $where[] = "cf.division_code = '$division_code'";
        }

        $orderBy = " order by date desc";

        $query = $query . implode(" and ", $where) . $orderBy;

        return DB::select($query);
    }


//temporary script

  function importCustomer(){
        $this->importCustomer1();
        $this->importCustomer2();
    }

    function importCustomer1(){
            $rows = DB::table('temp_customers')->where('parent_child','!=',null)->get();

            $data = collect($rows)->groupBy('parent_child');

//        return $data;

            foreach($data as $rows){
                $customer = new Customer();
                $customer->name = $rows[0]->name;
                $customer->save();

                //save codes
                foreach($rows as $i=>$row){

                    //find if code and division already exists if exists add as child
                    $check_exists = CustomerCode::where('customer_id','=',$customer->id)->where('division_code','=',$row->division_code)->exists();

                    if($check_exists && $i>0){ //create as child and add codes
                        $parent_id = $customer->id;
                        $child_customer = new Customer();
                        $child_customer->name = $row->name;
                        $child_customer->parent_id = $parent_id;
                        $child_customer->save();

                        $customer_code = new CustomerCode();
                        $customer_code->customer_id = $child_customer->id;
                        $customer_code->customer_code = $row->customer_code;
                        $customer_code->division_code = $row->division_code;
                        $customer_code->name = $row->name;
                        $customer_code->save();
                    }else{ //insert code
                        $customer_code = new CustomerCode();
                        $customer_code->customer_id = $customer->id;
                        $customer_code->customer_code = $row->customer_code;
                        $customer_code->division_code = $row->division_code;
                        $customer_code->name = $row->name;
                        $customer_code->save();
                    }

                }

            }

            return $data;
    }


    function importCustomer2(){
        $data = DB::table('temp_customers')->where('parent_child','=',null)->get();


        foreach($data as $row){
            $customer = new Customer();
            $customer->name = $row->name;
            $customer->save();

            $customer_code = new CustomerCode();
            $customer_code->customer_id = $customer->id;
            $customer_code->customer_code = $row->customer_code;
            $customer_code->division_code = $row->division_code;
            $customer_code->name = $row->name;
            $customer_code->save();
        }

        return $data;
    }

    function CashFlow(Request $request){

        $customerIds = CashFlow::distinct()
                        ->when($request->input('start_date') && $request->input('end_date'),function($q) use ($request){
                                    $start_date = $request->input('start_date');
                                    $end_date = $request->input('end_date');
                                    $q->whereBetween('date', [$start_date, $end_date])->get();
                                })

                        ->when($request->input('division_code'), function($q) use ($request){
                                $division_code = $request->input('division_code');
                                $q->where('division_code','=',$division_code);
                            })

                        ->get(['customer_id'])
                        ->pluck('customer_id')->toArray();




        //get customers with parentId as some customer might have cashflow for their child customer only
        $parentCustomerIds = Customer::whereIn('id',$customerIds)->distinct()->get(['id','parent_id'])->pluck('parent_id')->toArray();

        $customerIds = array_unique(array_merge($customerIds, $parentCustomerIds));



        $result = Customer::whereNull('parent_id')->with(['cashFlows'=>function($q) use ($request){
                $q->when($request->input('start_date') && $request->input('end_date'),function($q) use ($request){
                            $start_date = $request->input('start_date');
                            $end_date = $request->input('end_date');
                            $q->whereBetween('date', [$start_date, $end_date])->get();
                        });

                $q->when($request->input('division_code'), function($q) use ($request){
                        $division_code = $request->input('division_code');
                        $q->where('division_code','=',$division_code);
                    });

                                //apply acl by divisions
                $q->when(!Helper::is_admin(), function($q){
                    $q->whereIn('division_code',Helper::cast_null_to_array(Auth::user()->divisions));
                    });


                $q->orderBy('date','desc');
        },'children'=>function($q) use ($customerIds, $request){
            $q->whereIn('customers.id',$customerIds);
            $q->with(['cashFlows'=>function($q) use ($request){

                $q->when($request->input('start_date') && $request->input('end_date'),function($q) use ($request){
                    $start_date = $request->input('start_date');
                    $end_date = $request->input('end_date');
                    $q->whereBetween('date', [$start_date, $end_date])->get();
                     });
                
                $q->when($request->input('division_code'), function($q) use ($request){
                    $division_code = $request->input('division_code');
                    $q->where('division_code','=',$division_code);
                });

                //apply acl by divisions
                $q->when(!Helper::is_admin(), function($q){
                    $q->whereIn('division_code',Helper::cast_null_to_array(Auth::user()->divisions));
                    });

                $q->orderBy('date','desc');
            }]);
        }])
//apply acl part
        ->when(!Helper::is_admin(), function($q){
            $q->whereIn('id',Helper::cast_null_to_array(Auth::user()->customers));
            })
        ->when(Helper::is_admin(), function($q) use ($customerIds){
            $q->whereIn('id',$customerIds); // customerIds represents distinct customer in cashflow table so we show only customers having atleast on cashflow transaction where parent customer is also incuded just if only children has cashflow
            })

        ->orderBy('name')->get()->toArray();

        
        //calculate aggregate amount
        foreach($result as $i=>$customer){

            $this->calcAggregateAmount($result[$i]); //calc aggregate amount of parent customer

            foreach ($result[$i]['children'] as $k=>$childCustomer){
                $this->calcAggregateAmount($result[$i]['children'][$k]); // calc aggregate amount of children

                $result[$i]['amount'] = $result[$i]['amount'] + $result[$i]['children'][$k]['amount']; //sum aggergate amount of children
            }
        }

        return $result;
    }

    function calcAggregateAmount(&$customer){
        $customer['amount'] = null;
        $customer['currency_code'] = 'USD';
        $customer['collapsed'] = true;

        foreach($customer['cash_flows'] as $row){
            $customer['amount'] = $customer['amount'] + round(currency($row['amount'],$row['currency_code'],'USD',false),2);
        }
    }

    function exportAgingReport(Request $request){
        $data = $this->agingReport($request);


            
    
            $fileName = 'aging-report'.$request->user()->id.time();
            Excel::create($fileName, function($excel) use($data){

                             $excel->sheet('aging-report', function($sheet) use($data){
                                $row =['CustomerId','Name','Current','30 Days','60 Days','60 Days Plus','Total','Currency Code'];
                                $sheet->row(1,$row);

                                $i=2;
                                foreach($data['customers'] as $customer){
                                    $row = [
                                    'customerId'=>$customer['customer_id'],
                                    'name'=>$customer['name'],
                                    'current'=>$customer['current'],
                                    '30_days'=>$customer['thirty_days'],
                                    '60_days'=>$customer['sixty_days'],
                                    '60_days_plus'=>$customer['sixty_days_plus'],
                                    'total'=>$customer['total'],
                                    'currency_code'=>$customer['currency_code'],
                                    ];

                                    $sheet->row($i,$row);
                                    $i++;


                                    if(count($customer['agings'])>1)
                                        {
                                        foreach($customer['agings'] as $customer){
                                            $customer = (array) $customer;
                                            $row = [
                                                    'customerId'=>'',
                                                    'name'=>'',
                                                    'current'=>$customer['current'],
                                                    '30_days'=>$customer['thirty_days'],
                                                    '60_days'=>$customer['sixty_days'],
                                                    '60_days_plus'=>$customer['sixty_days_plus'],
                                                    'total'=>$customer['total'],
                                                    'currency_code'=>$customer['currency_code'],
                                                    ];

                                                $sheet->row($i,$row);
                                                $i++;
                                    
                                                }
                                        }
                                }
                                //add aggregate record
                                if(isset($data['aggregate'])){
                                    $i++;
                                    $data['aggregate']=(array)$data['aggregate'];
                                    
                                            $row = [
                                                    'customerId'=>'Aggregate',
                                                    'name'=>'',
                                                    'current'=>$data['aggregate']['current'],
                                                    '30_days'=>$data['aggregate']['thirty_days'],
                                                    '60_days'=>$data['aggregate']['sixty_days'],
                                                    '60_days_plus'=>$data['aggregate']['sixty_days_plus'],
                                                    'total'=>$data['aggregate']['total'],
                                                    'currency_code'=>$data['aggregate']['currency_code'],
                                                    ];

                                                $sheet->row($i,$row);
                                                $i++;
                                            }////////////////////////////////////

                                });

                    })->store('xls');
            $data = [];
            $data['link'] = url('api/download/'.$fileName.'.xls');
            return $data;

    }


}