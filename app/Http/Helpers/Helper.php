<?php
/**
 * Created by PhpStorm.
 * User: ruban
 * Date: 11/19/2017
 * Time: 8:40 PM
 */

namespace App\Http\Helpers;

class Helper {
    public static function is_admin(){
        return request()->user()->tokenCan('*');
    }

    public static function cast_null_to_array($arr){
        return is_null($arr)?[]:$arr;
    }
}
