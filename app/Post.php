<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'array',
        'body' => 'array',
        'excerpt' => 'array',
        'seo_title' => 'array',
        'seo_meta_key' => 'array',
        'seo_meta_description' => 'array'
    ];

    protected $fillable = ['title','body','excerpt','slug'];

}
