<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{


    const TYPE_AGING = 0;
    const TYPE_CASHFLOW = 1;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id', 'code', 'name', 'email', 'phone', 'fax', 'address', 'country', 'city', 'contact_person_name', 'contact_person_phone', 'contact_person_email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    public function codes(){
        return $this->hasMany('App\CustomerCode','customer_id','id');
    }

    public function comments(){
        return $this->hasMany('App\Comment','customer_id','id');
    }

    public function latestComment(){
        return $this->hasOne('App\Comment', 'customer_id', 'id')->latest();
    }

    public function children(){
        return $this->hasMany('App\Customer','parent_id');
    }

    public function agings(){
        return $this->hasMany('App\Aging','customer_id','id');
    }

    public function cashFlows(){
        return $this->hasMany('App\cashFlow','customer_id','id');
    }

}
