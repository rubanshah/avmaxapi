<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aging extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    public function customer(){
        return $this->belongsTo('App\Customer','customer_id','id');
    }

    public function parent(){
        return $this->belongsTo('App\Customer','customer_id','id')->where('customers.parent_id',null);
    }

    public function children(){
        return $this->hasManyThrough('App\Aging','App\Customer','parent_id','customer_id');
    }

}
