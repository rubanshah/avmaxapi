<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;


class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const NOT_VERIFIED = 0;
    const VERIFIED = 1;
    const SUSPENDED = 2;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'divisions' => 'array',
        'customers' => 'array'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone', 'country_code', 'city', 'divisions', 'customers'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];


    public function roles()
    {
        return $this->belongsToMany('App\Role','user_roles', 'user_id', 'role_id');
    }

    public function getRoles(){
        return explode(',',$this->roles()->select('role')->get()->implode('role',','));
    }

    public function getPermissions(){
        $permissions = DB::table('roles')
            ->join('role_perm', function($join){
                $join->on('roles.id','=','role_perm.role_id');
            })->join('permissions',function($join){
                $join->on('role_perm.perm_id','=','permissions.id');
            })->select('permissions.perm_name')
            ->whereIn('role',$this->getRoles())
            ->get();

        return explode(',',$permissions->implode('perm_name',','));
    }

}
