<?php
namespace App\Repositories;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewUser;

class UserRepository {
    use ValidatesRequests;

    public function getAll()
    {
        return User::with('roles')->paginate(1000);
    }

    public function find($id){
        return User::with('roles')->find($id);
    }

    public  function create($data){
        $validation = Validator::make($data,['first_name' => 'required',
            'last_name' => 'required',
            'email'=> "required|email|unique:users",
            'country_code'=> 'required',
            'city'=> 'required',
        ])->validate();

        $user = new User();
        $user->fill($data);

        //set random password and then send email to that user
        //set password
        $password = str_random(8);

        $user->password = Hash::make($password);
        $user->status = 1;
        $user->save();
        
        Mail::to($user)->send(new NewUser($user,$password));

        if(isset($data['roles'][0]) && isset($data['roles'][0]['id'])){
            $user->roles()->sync($data['roles'][0]['id']);
        }

        return $user;
    }

    public function update($id,$data){
        $validation = Validator::make($data,['first_name' => 'required',
            'last_name' => 'required',
            'email'=> "required|email|unique:users,id,$id",
            'country_code'=> 'required',
            'city'=> 'required',

        ])->validate();

        $user = User::find($id);
        $user->fill($data);
        $user->save();

        if(isset($data['roles'][0]) && isset($data['roles'][0]['id'])){
            $user->roles()->sync($data['roles'][0]['id']);
        }
        return $user;
    }


    public function updatePermission($id,$data){
        $user = User::find($id);
        $user->fill($data);
        $user->save();
        return $user;
    }

    public function changePassword($id, $data){

         $validation = Validator::make($data,['password' => 'required|min:6',
            'password_confirm' => 'required|same:password',
            

        ])->validate();

        $user = User::find($id);
        $user->password = bcrypt($data['password']);
        $user->save();
        return $user;
    }
}