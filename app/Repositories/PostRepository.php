<?php
namespace App\Repositories;

use App\Post;
use Illuminate\Support\Facades\Validator;

class PostRepository {

    private $rules = ['title.*' => 'required',
        'slug' => 'required|unique:posts|max:255',
        'body.*' => 'required',
        'excerpt.*' => 'required'
    ];

    public function getAll()
    {
        return Post::paginate(5);
    }

    public function find($id){
        return Post::find($id);
    }

    public function create($data=array()){
        Validator::make($data,$this->rules)->validate();
        return Post::create($data);
    }

    public function update(){

    }

}