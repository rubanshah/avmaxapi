<?php
namespace App\Repositories;

use App\Aging;
use App\Customer;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use App\CashFlow;
use DateTime;

class ReportRepository {


    public function agingReport($filter)
    {

        $query = Aging::has('parent')->with(['customer'=>function($q){
                $q->select('id','name');
                $q->where('parent_id',null);
                $q->with('latestComment');
        },'children'=>function($q){
                $q->with(['customer'=>function($q){
                    $q->select('id','name');
                    $q->with('latestComment');
                }]);
            }]);

        if(isset($filter['division']) && $filter['division']){
            $division = $filter['division'];
            $query->where('division_code',$division);
        }

        $agingCustomersList = Aging::select('customer_id')->distinct()->get()->pluck('customer_id');

        $query = Customer::with(['agings','latestComment','children.agings','children.latestComment'])->where('parent_id','=',null);

        $query = $query->whereIn('id',$agingCustomersList)->orderBy('name');

        $data = $query->get();
        return $data;
        return ['data'=>$data];
    }

    public function validateAging($data){
        $data = $this->createTemporaryAgingTable($data);

        $hasError = false;

        foreach($data as $row){
            if(!$row->customer_id){ //if customer_codes.id is null
                $row->error = 'CUSTOMER_NOT_FOUND';
                $hasError = true;
            }
            unset($row->customer_id);
        }

        if($hasError){
            return response()->json($data,422);
        }else{
            return $data;
        }

    }

 
    //returns data along with customer_id
    function createTemporaryAgingTable($data){
        Schema::create('temp_aging', function (Blueprint $table) {
            $table->string('customer_code',10)->nullable();
            $table->string('currency_code',5)->nullable();
            $table->string('division_code',10)->nullable();
            $table->string('customer_name',50)->nullable();
            $table->double('current',10,2);
            $table->double('thirty_days',10,2);
            $table->double('sixty_days',10,2);
            $table->double('sixty_days_plus',10,2);
            $table->temporary();
        });


        //filter unwanted fields from collection
        $fillable = ['customer_code','currency_code','division_code','customer_name','current','thirty_days','sixty_days','sixty_days_plus'];

        foreach($data as $i=>$row){
            foreach($row as $k=>$value){
                if(!in_array($k,$fillable)){
                    unset($data[$i][$k]);
                    $data[$i]['customer_name'] = strtoupper($data[$i]['customer_name']); //convert name to upper case
                }
            }
        }
        //eof key filtering

        DB::table('temp_aging')->insert($data);

        $data = DB::table('temp_aging')
            ->leftjoin('customer_codes', function($join){
                $join->on('temp_aging.customer_code','=','customer_codes.customer_code')
                    ->on('temp_aging.division_code','=','customer_codes.division_code');
            })->select('temp_aging.*','customer_codes.customer_id')->get();

        return $data;
    }



    function importAging($data){

        $data = $this->createTemporaryAgingTable($data);

        foreach($data as $i=>$row){
            unset($row->customer_code);
            unset($row->customer_name);
        }

        $data = collect($data)->map(function($x){ return (array) $x; })->toArray(); //convert collection to array of arrays

        Aging::truncate();
        Aging::insert($data);
        return 'true';
    }



   public function validateCashFlow($data){

        $data = $this->createTemporaryCashFlowTable($data);

        $hasError = false;

        foreach($data as $row){
            if(!$row->customer_id){
                $row->error = 'CUSTOMER_NOT_FOUND';
                $hasError = true;
            }

         //   unset($row->customer_id);
            
        }

        if($hasError){
            return response()->json($data,422);
        }else{
            return $data;
        }
    }


    function createTemporaryCashFlowTable($data){
          Schema::create('temp_cash_flow', function (Blueprint $table) {
            $table->string('name',100)->nullable();
            $table->string('amount',10)->nullable();
            $table->string('division_code',10)->nullable();
            $table->string('currency_code',10)->nullable();
            $table->string('customer_code',50)->nullable();
            $table->date('date');
            $table->temporary();
        });


             //filter unwanted fields from collection
        $fillable = ['customer_code','name','amount','division_code','currency_code','date'];

        foreach($data as $i=>$row){
            foreach($row as $k=>$value){
                if(!in_array($k,$fillable)){
                    unset($data[$i][$k]);
                    $data[$i]['name'] = strtoupper($data[$i]['name']); //convert name to upper case
                }
            }
        }
        //eof key filtering

        DB::table('temp_cash_flow')->insert($data);


        $query = "SELECT t.*, cc.customer_id FROM temp_cash_flow t LEFT JOIN customer_codes cc ON (t.division_code=cc.division_code && t.name=cc.name && cc.type=1)";

        $result = DB::select($query);

        $collection = collect($result);


        return $collection;

    }

    function importCashFlow($r){
         $data = $this->createTemporaryCashFlowTable($r['data']);
         $start_date = $r['date']['start_date'];
         $end_date = $r['date']['end_date'];


        foreach($data as $i=>$row){
            unset($row->customer_code);
            }

        $data = collect($data)->map(function($x){ return (array) $x; })->toArray(); //convert collection to array of arrays
        
        $query = "delete from cash_flow where date between :start_date and :end_date";

        $result = DB::delete($query, [$start_date, $end_date]);


        foreach ($data as $i=>$row) {
            $d = strtotime($row['date']);
            $s = strtotime($start_date);
            $e = strtotime($end_date);

            //check if $d in between
            if($d >= $s && $d <= $e){
                //in between
            }else{
                unset($data[$i]);
            }
        }
       
        CashFlow::insert($data);
        return 'true';
    }

}