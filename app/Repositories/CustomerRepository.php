<?php
namespace App\Repositories;

use App\Customer;
use App\Comment;
use App\CustomerCode;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\DB;

use App\Http\Helpers\Helper;
class CustomerRepository {

    use ValidatesRequests;

    public function getAll($search=null)
    {
        if($search){
            return Customer::where('name','like',"%$search%")->when(!Helper::is_admin(), function($q){
                    $q->whereIn('id',Helper::cast_null_to_array(Auth::user()->customers));
            })->paginate(5);
        }
        return Customer::when(!Helper::is_admin(), function($q){
            $q->whereIn('id',Helper::cast_null_to_array(Auth::user()->customers));
        })->paginate(5);
    }

    public  function parentCustomer(){
        return Customer::where('parent_id','=',null)->get();
    }

    public function getChildren($id){
        return Customer::with('children.codes')->find($id);
    }

    public function find($id){
        return Customer::find($id);
    }

    public function findWithCodes($id){
        return Customer::with('codes')->find($id);
    }

    public function create($data){
     /*   $validation = Validator::make($data,['name' => 'required',
                                        'email'=> 'required|email',
                                        'phone'=> 'required',
                                        'address'=> 'required',
                                        'country'=> 'required',
                                        'city'=> 'required',
                                        'contact_person_email'=> 'email',

                                        ])->validate();
                                        */


        //$validation = Validator::make($data,['name' => 'required'])->validate();

         /*
         $validation = Validator::make($data,['name' => 'required',
            'codes.*.division_code'=> 'required',
            'codes.*.name' => 'required',
            'codes.*.customer_code' => 'required'
            ])->validate();
            */

         $code_rules = [];
         foreach($data['codes'] as $i=>$code){
            if($code['type']==0){
            $code_rules["codes.{$i}.customer_code"] = 'unique_code:'.$code['division_code'];
            $code_rules["codes.{$i}.name"] = 'required';

            }else{
            $code_rules["codes.{$i}.name"] = 'required | unique_name:'.$code['division_code'];
            }


            $code_rules["codes.{$i}.division_code"] = 'required';

         }

          $validator = Validator::make($data,array_merge(['name' => 'required',
            'codes.*.name' => 'required'
            ],$code_rules))->validate();         

        $customer = new  Customer();
        $customer->fill($data);
        $customer->save();

        //save codes
        $codes = $data['codes'];
       
        //create codes
        foreach($codes as $row)
        {
                $code = new CustomerCode();
                $code->fill($row);
                $code->customer_id = $customer->id;
                $code->save();
        }
        return $customer;
    }

    public function update($id,$data){
      /*  $validation = Validator::make($data,['name' => 'required',
            'email'=> 'required|email',
            'phone'=> 'required',
            'address'=> 'required',
            'country'=> 'required',
            'city'=> 'required',
            'contact_person_email'=> 'email',

        ])->validate();*/

        //$validation = Validator::make($data,['name' => 'required'])->validate();

         $code_rules = [];
         foreach($data['codes'] as $i=>$code){
            if($code['type']==0){
            $code_rules["codes.{$i}.customer_code"] = 'required | unique_code:'.$code['division_code'].','.$code['id'];
            $code_rules["codes.{$i}.name"] = 'required';

            }else{
            $code_rules["codes.{$i}.name"] = 'required | unique_name:'.$code['division_code'].','.$code['id'];
            }


            $code_rules["codes.{$i}.division_code"] = 'required';

         }

          $validator = Validator::make($data,array_merge(['name' => 'required',
            
            'codes.*.customer_id' => 'required',
            'codes.*.name' => 'required'
            ],$code_rules))->validate();



        $customer = Customer::find($id);
        $customer->fill($data);
        $customer->save();

        //save codes
        $codes = $data['codes'];

        //delete codes
        DB::table('customer_codes')->where('customer_id','=',$customer->id)->whereNotIn('id',array_pluck($codes,'id'))->delete();

        //create update codes
        foreach($codes as $row)
        {
            $code = CustomerCode::find($row['id']);
            if($code){
                $code->fill($row);
                $code->customer_id = $customer->id;
                $code->save();
            }
            else{
                $code = new CustomerCode();
                $code->fill($row);
                $code->customer_id = $customer->id;
                $code->save();
            }
        }


        return $customer;
    }

    public function delete($id){
       CustomerCode::where('customer_id','=',$id)->delete();
       return Customer::destroy($id);
    }

    public function getComments($customerId){

        return Comment::with(['user'=>function($q){
            $q->select('id','username','first_name','last_name');
        }])->where('customer_id',$customerId)->get();

        return Customer::with(['comments' => function($q){
            $q->with(['user'=>function($q){
                $q->select('id','username','first_name','last_name');
            }]);
        }])->where('id',$customerId)->select('id','code','name')->get();
    }

    function createComment($customerId,$data){
        $validation = Validator::make($data,['text' => 'required'])->validate();

        $comment = new Comment();
        $comment->user_id = Auth::user()->id;
        $comment->customer_id = $customerId;
        $comment->text = $data['text'];

        $comment->save();
        return $comment;
    }

    function deleteComment($id){
        return Comment::destroy($id);
    }

    function getComment($id){
        return Comment::find($id);
    }

    function updateComment($id,$data){
        $validation = Validator::make($data,['text' => 'required'])->validate();

        $comment=Comment::find($id);
        $comment->text = $data['text'];
        $comment->save();
        return $comment;
    }

    function exportCustomerList(){
        return Customer::with(['codes','children.codes'])->where('parent_id','=',null)->get();
    }

}