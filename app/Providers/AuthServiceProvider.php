<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;
use Carbon\Carbon;


use App\Comment;
use App\Policies\CommentPolicy;
use App\Policies\UserPolicy;
use App\Policies\CustomerPolicy;
use App\Customer;
use App\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Comment::class => CommentPolicy::class,
        User::class => UserPolicy::class,
        Customer::class => CustomerPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        Passport::routes();
        Passport::tokensExpireIn(Carbon::now()->addDays(15));
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));

        Passport::tokensCan([
            'view-customer' => 'View Customer',
            'add-customer' => 'View Customer',
            'update-customer' => 'View Customer',
            'view-comment' => 'View Customer',
            'add-comment' => 'View Customer',
            'update-comment' => 'View Customer',

            'view-report' => 'View Customer',
            'view-user' => 'View Customer',
            'add-user' => 'View Customer',
            'update-user' => 'View Customer',
            'update-permission' => 'Update Permission'
        ]);
    }
}
