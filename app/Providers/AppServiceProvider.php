<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\CustomerCode;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //param0 -division_code  param1 ignore id
         Validator::extend('unique_code', function ($attribute, $value, $parameters, $validator) {
            $query = CustomerCode::where('division_code','=',$parameters[0])
            ->where('customer_code','=',$value);
            
            if(isset($parameters[1])){
                $query->where('id','!=',$parameters[1]);
            }

            $count = $query->count();
            return $count>0? false:true;
        },'Duplicate Code');


         Validator::extend('unique_name', function ($attribute, $value, $parameters, $validator) {
            $query = CustomerCode::where('division_code','=',$parameters[0])
            ->whereNull('customer_code')
            ->where('name','=',$value);
            
            if(isset($parameters[1])){
                $query->where('id','!=',$parameters[1]);
            }

            $count = $query->count();
            return $count>0? false:true;
        },'Duplicate Name');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
