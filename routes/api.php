<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::get('/import_customers','ReportController@importCustomer');

Route::get('/roles', function(){
    return App\Role::all();
});


Route::post('/login', 'LoginController@login');
Route::get('/reset-password', 'LoginController@resetPassword');

Route::middleware('auth:api')->get('/auth/me', function (Request $request) {
    return Auth::user();
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return Auth::user();
    return $request->user();
});



Route::middleware('auth:api')->get('/users', 'UserController@index')->middleware('can:lists,App\User');
Route::middleware('auth:api')->get('/users/{id}', 'UserController@find')->middleware('can:view,App\User,id');
Route::middleware('auth:api')->post('/users', 'UserController@create')->middleware('can:create,App\User,id');
Route::middleware('auth:api')->patch('users/{id}', 'UserController@update')->middleware('can:update,App\User');
Route::middleware('auth:api')->patch('users/update-permission/{id}', 'UserController@updatePermission')->middleware('can:updatePermission,App\User');
Route::middleware('auth:api')->patch('users/{id}/change-password', 'UserController@changePassword')->middleware('can:changePassword,App\User,id');


Route::middleware('auth:api')->get('/customers', 'CustomerController@index');
Route::middleware('auth:api')->get('/customers/parent', 'CustomerController@parentCustomer');
Route::get('/customersWithRelations', 'CustomerController@customersWithRelations');



Route::middleware('addAccessToken')->middleware('auth:api')->get('/customers/export-customer-list','CustomerController@exportCustomerList');


Route::middleware('auth:api')->get('/customers/{id}', 'CustomerController@find');
Route::middleware('auth:api')->post('/customers', 'CustomerController@create')->middleware('can:create,App\Customer');
Route::middleware('auth:api')->patch('/customers/{id}', 'CustomerController@update')->middleware('can:update,App\Customer,id');
Route::middleware('auth:api')->delete('/customers/{id}', 'CustomerController@delete')->middleware('can:delete,App\Customer');

Route::middleware('auth:api')->get('/customers/{id}/children', 'CustomerController@getChildren');



Route::middleware('auth:api')->get('/customers/{id}/comments', 'CustomerController@getComments');
Route::middleware('auth:api')->post('customers/{id}/comments', 'CustomerController@createComment')->middleware('scope:add-comment');
Route::middleware('auth:api')->delete('comments/{id}', 'CustomerController@deleteComment')->middleware('can:delete,App\Comment,id');
Route::middleware('auth:api')->get('comments/{id}', 'CustomerController@getComment');
Route::middleware('auth:api')->patch('comments/{id}', 'CustomerController@updateComment')->middleware('can:update,App\Comment,id');


Route::middleware('auth:api')->get('reports/aging', 'ReportController@aging');//old one to remove

Route::middleware('auth:api')->get('reports/agingReport', 'ReportController@agingReport');
Route::middleware('auth:api')->post('import/validate-aging', 'ReportController@validateAging')->middleware('can:import,App\User');
Route::middleware('auth:api')->post('import/import-aging', 'ReportController@importAging')->middleware('can:import,App\User');


Route::middleware('auth:api')->get('reports/cash-flow-report', 'ReportController@cashFlowReport'); //old route

Route::get('reports/customers-for-cash-flow', 'ReportController@customersForCashFlow');
Route::get('reports/customer-cash-flow/{id}', 'ReportController@customerCashFlow');

Route::middleware('auth:api')->get('reports/cash-flow', 'ReportController@CashFlow');

Route::middleware('auth:api')->post('import/validate-cash-flow', 'ReportController@validateCashFlow');
Route::middleware('auth:api')->post('import/import-cash-flow', 'ReportController@importCashFlow');



Route::middleware('auth:api')->post('import/export-aging-invalid-data', 'ReportController@exportAgingInvalidData');
Route::middleware('auth:api')->post('import/export-cash-flow-invalid-data', 'ReportController@exportCashFlowInvalidData');

Route::middleware('auth:api')->get('reports/aging/export', 'ReportController@exportAgingReport');


Route::get('download/{filename}', function($filename)
{
    // Check if file exists in app/storage/file folder
    $file_path = storage_path() .'/exports/'. $filename;
    if (file_exists($file_path))
    {
        // Send Download
        return Response::download($file_path, $filename, [
            'Content-Length: '. filesize($file_path)
        ]);
    }
    else
    {
        // Error
        exit('Requested file does not exist on our server!');
    }
});


Route::get('settings/currency-exchange-rates','SettingsController@getCurrencyExchangeRates');

Route::patch('settings/currency-exchange-rates','SettingsController@updateCurrencyExchangeRates');

//create and edit method sends view so discard those
Route::middleware('auth:api')->resource('posts','PostController',['except' => [
    'create', 'edit'
]]);