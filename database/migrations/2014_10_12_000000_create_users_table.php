<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username',25)->unique()->nullable();
            $table->string('first_name',25)->nullable();
            $table->string('last_name',25)->nullable();
            $table->string('email',50)->unique();
            $table->string('password');
            $table->tinyInteger('status')->default(0)->comment('0-not verified, 1-verified, 2-suspended');

            $table->rememberToken();
            $table->ipAddress('ip_address')->nullable();

            $table->string('country_code',5)->nullable();
            $table->string('city',50)->nullable();
            $table->string('state',50)->nullable();
            $table->string('zip',10)->nullable();

            $table->string('image',50)->nullable();

            $table->string('designation',50)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
