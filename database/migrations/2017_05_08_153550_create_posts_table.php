<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->nullable();

            $table->string('slug',50)->unique();
            $table->integer('author')->nullable();

            $table->json('title');
            $table->json('body');
            $table->json('excerpt');

            $table->json('seo_title')->nullable();
            $table->json('seo_meta_key')->nullable();
            $table->json('seo_meta_description')->nullable();

            $table->enum('type',['page','post'])->default('page');
            $table->tinyInteger('status')->default(0)->comment('0-Not Published, 1-Published');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
