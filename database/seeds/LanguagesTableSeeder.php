<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->truncate();
        DB::table('languages')->insert([[
                'code' => 'en',
                'name' => 'English'
            ],
            [
                'code' => 'nl',
                'name' => 'Dutch'
            ],
            [
                'code' => 'no',
                'name' => 'Norwegian'
            ],
            [
                'code' => 'fi',
                'name' => 'Finnish'
            ]]
            );
    }
}
