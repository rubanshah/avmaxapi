<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use Faker\Provider\Lorem;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->truncate();

        $faker = Faker::create();
        $faker->addProvider(new Lorem($faker));


        $post = new App\Post;
        
            $post->slug = 'about-us';

            $title = $post->title;
            $title['en'] = 'About Us';
            $title['fi'] = 'About Us';
            $post->title = $title;

            $description = $post->description;
            $description['en'] = $faker->text(2000);
            $description['fi'] = $faker->text(2000);
            $post->description = $description;


            $excerpt = $post->excerpt;
            $excerpt['en'] =  $faker->paragraph($nbSentences = 5, $variableNbSentences = true);
            $excerpt['fi'] =  $faker->paragraph($nbSentences = 5, $variableNbSentences = true);
            $post->excerpt = $excerpt;


            $post->status = '1';

            $post->save();


    }
}
